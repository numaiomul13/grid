[
	{
		"id": 0,
		"name": "Residence",
		"limit": -1,
		"size": 33,
		"resourcePath":"residence",
		"cost":{
			"time": 10,
			"value":"100.G"
		}
		"production": {
			"time": 10,
			"value":"100.G",
			"auto":true
		}
	},
	{
		"id": 1,
		"name": "Wood Workshop",
		"limit": -1,
		"size": 32,
		"resourcePath":"woodWorkshop",
		"cost":{
			"time": 10,
			"value":"150.G"
		}
		"production": {
			"time": 10,
			"value":"50.W",
			"auto":false
		}
	},
	{
		"id": 2,
		"name": "Steel Workshop",
		"limit": -1,
		"size": 22,
		"resourcePath":"steelWorkshop",
		"cost":{
			"time": 10,
			"value":"150.G|100.W"
		}
		"production": {
			"time": 10,
			"value":"50.S",
			"auto":false
		}
	},
	{
		"id": 3,
		"name": "Bench",
		"limit": -1,
		"size": 11,
		"resourcePath":"bench",
		"cost":{
			"time": 10,
			"value":"150.G|50.S"
		}
	},
	{
		"id": 4,
		"name": "Tree",
		"limit": -1,
		"size": 22,
		"resourcePath":"tree",
		"cost":{
			"time": 10,
			"value":"50.G|200.W"
		}
	}
]