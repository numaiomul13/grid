﻿public struct Resource 
{
	public enum TYPE {
		GOLD,
		STEEL,
		WOOD
	}

	public TYPE type;
	public float quantity;

	public Resource (string _value) {
		string[] args = _value.Split('.');
		quantity = float.Parse(args[0]);
		type = FromString(args[1]);
	}
	public Resource (Resource _toCopy) {
		type = _toCopy.type;
		quantity = _toCopy.quantity;
	}
	public static TYPE FromString(string _value) {
		switch (_value) {
			case "G":return TYPE.GOLD;
			case "S":return TYPE.STEEL;
			case "W":return TYPE.WOOD;
			default:throw new System.Exception("Resource " + _value + " not supported!");
		}
	}

	public override string ToString() {
		return quantity.ToString("0. ") + type.ToString();
	}

	public static Resource operator +(Resource a, Resource b) {
		if (a.type != b.type) {
			throw new System.Exception("Can not add resource type " + a.type + " with "+b.type);
		}
		Resource retVal = new Resource();
		retVal.type = a.type;
		retVal.quantity = a.quantity + b.quantity;
		return retVal;
	}
	public static Resource operator -(Resource a, Resource b) {
		if (a.type != b.type) {
			throw new System.Exception("Can not subtract resource type " + a.type + " with " + b.type);
		}
		Resource retVal = new Resource();
		retVal.type = a.type;
		retVal.quantity = a.quantity - b.quantity;
		return retVal;
	}
}
