﻿using UnityEngine;
using UnityEngine.UI;

public class ResourcesMono : MonoBehaviour {
	public Text gold;
	public Text wood;
	public Text steel;

	void Awake() {
		ResourcesLogic.Register(UpdateInfo);
	}

	void UpdateInfo(float _gold, float _wood, float _steel) {
		gold.text = _gold.ToString("0.");
		wood.text = _wood.ToString("0.");
		steel.text = _steel.ToString("0.");
	}
}
