﻿using SimpleJSON;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class ResourcesLogic {
	public delegate void OnValueChanged(float _gold, float _wood, float _steel);

	static Dictionary<Resource.TYPE, float> wallet = new Dictionary<Resource.TYPE, float>();
	static List<OnValueChanged> listeners = new List<OnValueChanged>();

	const string PREFS_KEY = "resourcesSave";

	public static void Start() {
		if (BuildingsInventory.CLEAR_LOAD) {
			LoadDefault();
		} else {
			Load();
		}
		NotifyListeners();
	}

	public static void Add(params Resource[] _res) {
		foreach (Resource res in _res) {
			Add(res);
		}
	}
	public static void Remove(params Resource[] _res) {
		foreach(Resource res in _res) {
			Resource toAdd = new Resource(){ type = res.type,quantity = -1* res.quantity};
			Add(toAdd);
		}
	}
	public static void Add(Resource _res) {
		if (!wallet.ContainsKey(_res.type)) {
			wallet.Add(_res.type, 0f);
		}
		if (wallet[_res.type] + _res.quantity < 0) {
			throw new OperationCanceledException("We do not have enough resources for this operation");
		}
		wallet[_res.type] += _res.quantity;
		Save();
		NotifyListeners();
	}


	/// <summary>
	/// ASSUMPTION: each Resource entry type will be unique, the system may break if you send duplicate resource types
	/// </summary>
	/// <param name="res"></param>
	/// <returns></returns>
	public static bool CanRemove(params Resource[] _res) {
		foreach (Resource res in _res) {
			if (wallet[res.type] - res.quantity < 0) {
				return false;
			}
		}
		return true;
	}

	public static void Register(OnValueChanged _listener) {
		listeners.Add(_listener);
	}
	public static void Unregister(OnValueChanged _listener) {
		listeners.Remove(_listener);
	}

	static void NotifyListeners() {
		for (int i = listeners.Count - 1; i >= 0; i--) {
			listeners[i](wallet[Resource.TYPE.GOLD], wallet[Resource.TYPE.WOOD], wallet[Resource.TYPE.STEEL]);
		}
	}

	static void Load() {
		if (PlayerPrefs.HasKey(PREFS_KEY)) {
			JSONArray tmpArray = JSON.Parse(PlayerPrefs.GetString(PREFS_KEY)).AsArray;
			foreach (JSONObject tmpItem in tmpArray) {
				wallet.Add((Resource.TYPE)Enum.Parse(typeof(Resource.TYPE), tmpItem["type"].Value), float.Parse(tmpItem["value"]));
			}
		}
	}
	static void LoadDefault() {
		//load default values
		wallet.Add(Resource.TYPE.GOLD, 500f);
		wallet.Add(Resource.TYPE.WOOD, 200f);
		wallet.Add(Resource.TYPE.STEEL, 50f);
		Save();
	}
	static void Save() {
		//we save as json so it's easy to export to file/server etc
		JSONArray tmpArray = new JSONArray();
		JSONObject tmpObject;
		foreach (KeyValuePair<Resource.TYPE, float> elem in wallet) {
			tmpObject = new JSONObject();
			tmpObject.Add("type", elem.Key.ToString());
			tmpObject.Add("value", elem.Value);
			tmpArray.Add(tmpObject);
		}
		PlayerPrefs.SetString(PREFS_KEY, tmpArray.ToString());
	}
}