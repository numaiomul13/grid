﻿using UnityEngine;
public class Tile : MonoBehaviour
{

    public static Tile mouseOver;
    SpriteRenderer spriteRendererRef;

    public BuildingMono buildingRef;

    private Vector2Int pos;
    private Tile right, left, up, down; // x+,x-,y+,y-

    private void Awake()
    {
        spriteRendererRef = GetComponent<SpriteRenderer>();
    }

    public void SetHovered(bool _value)
    {
        if (_value)
        {
            spriteRendererRef.color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
        }
        else
        {
            spriteRendererRef.color = new Color(1f, 1f, 1f, 1f);
        }
    }

    private void OnMouseEnter()
    {
        mouseOver = this;
    }

    public void SetPos(int _x, int _y) {
        pos.Set(_x,_y);
    }
    public Vector2Int GetPos() {
        return pos;
    }
    public void SetLeft(Tile _neighbour)
    {
        left = _neighbour;
        left.SetRight(this);
    }
    public void SetDown(Tile _neighbour)
    {
        down = _neighbour;
        down.SetUp(this);
    }

    void SetRight(Tile _neighbour)
    {
        right = _neighbour;
    }
    void SetUp(Tile _neighbour)
    {
        up = _neighbour;
    }
    
    /// <summary>
    /// Get Neighbours going to the right and down
    /// </summary>
    /// <param name="_size">size of area to try to select</param>
    /// <param name="_withBuildings">should we get selection with occupied tiles</param>
    /// <returns>returns null if specified area is invalid (too large or is occupied with buildings</returns>
    public Tile[] TryGetNeighboursArea(Vector2Int _size, bool _withBuildings = false)
    {
        Tile[] retVal = new Tile[_size.x * _size.y];
        Tile tmpRow = this;
        for (int i = 0; i < _size.x; i++)
        {
            if (tmpRow == null) return null;
            Tile tmpCol = tmpRow;
            for (int j = 0; j < _size.y; j++)
            {
                if (tmpCol == null) return null;
                if(tmpCol.buildingRef != null && !_withBuildings) return null;

                retVal[i * _size.y + j] = tmpCol;
                tmpCol = tmpCol.down;
            }
            tmpRow = tmpRow.right;
        }
        return retVal;
    }
}
