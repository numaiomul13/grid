﻿using UnityEngine;
using System.Collections;
public class GridCreator : MonoBehaviour
{

    public Tile gridPrefab;
    public Vector2Int size;
    Tile[,] tiles;

    void Awake()
    {
        BuildGrid();
    }

    void BuildGrid()
    {
        tiles = new Tile[size.x, size.y];
        Tile tmpTile;
        for (int i = 0; i < size.x; i++)
        {
            for (int j = 0; j < size.y; j++)
            {
                tmpTile = Instantiate<Tile>(gridPrefab);
                tmpTile.transform.SetPositionAndRotation(new Vector3(tmpTile.transform.localScale.x * (i - size.x / 2), 0f, tmpTile.transform.localScale.y * (j - size.y / 2)), Quaternion.Euler(90f, 0f, 0f));
                tiles[i,j] = tmpTile;
                tmpTile.SetPos(i,j);
                if (i != 0)
                {
                    tmpTile.SetLeft(tiles[i - 1, j]);
                }
                if (j != 0)
                {
                    tmpTile.SetDown(tiles[i, j - 1]);
                }
            }
        }
    }
    public Tile GetAt(int _indexX,int _indexY) {
        return tiles[_indexX,_indexY];
    }
}