﻿using UnityEngine;

public class CameraMove : MonoBehaviour
{
    private static CameraMove instance;

    public Camera cameraRef;
    public float mouseModif;


    private void Awake()
    {
        if (instance != null)
        {
            throw new System.ApplicationException("Another instance already exists!");
        }
        instance = this;
    }

    public class ViewState : IGameState
    {

        Vector3 prevPos;
        Vector3 deltaPos;

        public void OnEnter()
        {
            prevPos = Input.mousePosition;
        }
        public void UpdateLoop(float _deltaTime)
        {
            if (Input.GetMouseButtonDown(0))
            {
                prevPos = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0))
            {
                deltaPos = prevPos - Input.mousePosition;
                deltaPos *= instance.mouseModif;
                instance.cameraRef.transform.Translate(deltaPos.x, 0f, deltaPos.y, Space.World);
                prevPos = Input.mousePosition;
            }
        }
        public void OnExit()
        {}
    }
}
