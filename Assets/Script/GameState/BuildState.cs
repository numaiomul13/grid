﻿
using UnityEngine;

public class BuildState : BuildingInteractionState {
	BuildingMono buildMono;
	BuildingData buildData;
	bool buildingPlaced;

	public BuildState(BuildingData _newBuilding) {
		building = GameObject.Instantiate(Resources.Load<GameObject>("Buildings/" + _newBuilding.resourcePath));
		building.GetComponent<BoxCollider>().enabled = false;//this will allow mouse to hit the grid for better building placement
		buildMono = building.AddComponent<BuildingMono>();
		size = new Vector2Int(_newBuilding.size.x, _newBuilding.size.y);
		buildData = _newBuilding;
	}
	public override void OnExit() {
		base.OnExit();
		if (!buildingPlaced) {
			GameObject.Destroy(building);
		}
	}
	protected override void OnLocationSelected() {
		buildingPlaced = true;
		building.GetComponent<BoxCollider>().enabled = true;
		BuildingsInventory.Build(buildMono, buildData, tileOver, size);

	}

}

