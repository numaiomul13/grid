﻿public class LoadingState : IGameState {
	public void OnEnter() {
		BuildingsInventory.Start();
		ResourcesLogic.Start();
	}

	public void UpdateLoop(float _deltaTime) {
		GameState.SetState(new CameraMove.ViewState());
	}
	
	public void OnExit() {}
}
