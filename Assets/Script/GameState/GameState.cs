﻿using UnityEngine;
using System.Collections.Generic;

public class GameState : MonoBehaviour {
	public delegate void UpdateListener(float _deltaTime);

	static GameState instance;

	IGameState currentState;
	List<UpdateListener> listeners;

	void Awake() {
		if (instance != null) {
			throw new System.ApplicationException("Another instance already exists!");
		}
		instance = this;
		listeners = new List<UpdateListener>();
	}

	void Start() {
		currentState = new LoadingState();
		currentState.OnEnter();
	}

	void Update() {
		currentState.UpdateLoop(Time.deltaTime);
		//we iterate in reverse since we want to register/unregister while iterating and not mess up the list order or skip elements
		for (int i = listeners.Count - 1; i >= 0; i--) {
			listeners[i](Time.deltaTime);
		}
	}

	public static void SetState(IGameState _newState) {
		instance.SetStateInternal(_newState);
	}
	void SetStateInternal(IGameState _newState) {
		currentState.OnExit();
		currentState = _newState;
		currentState.OnEnter();
	}

	public static System.Type GetState() {
		return instance.currentState.GetType();
	}

	public static void Register(UpdateListener _listener) {
		instance.listeners.Add(_listener);
	}
	public static void Unregister(UpdateListener _listener) {
		instance.listeners.Remove(_listener);
	}
}
