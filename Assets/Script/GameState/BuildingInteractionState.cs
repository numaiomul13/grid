﻿using UnityEngine;

public abstract class BuildingInteractionState : IGameState {

	protected GameObject building;
	protected Tile tileOver;
	protected Vector2Int size;

	public virtual void OnEnter() {
		if (Tile.mouseOver != null) {
			SnapBuildingTo(Tile.mouseOver);
		}
	}
	public virtual void UpdateLoop(float _deltaTime) {
		if (tileOver != Tile.mouseOver) {
			SnapBuildingTo(Tile.mouseOver);
		}
		if (Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButtonDown(1)) {
			GameState.SetState(new CameraMove.ViewState());
		}
		if (Input.GetMouseButtonDown(0)) {
			if (tileOver != null) {
				OnLocationSelected();
			}
			GameState.SetState(new CameraMove.ViewState());
		}
	}
	public virtual void OnExit() {
		if (tileOver != null) {
			Tile[] tmpOldTiles = tileOver.TryGetNeighboursArea(size, true);
			foreach (Tile tmpTile in tmpOldTiles) {
				tmpTile.SetHovered(false);
			}
		}
	}

	protected abstract void OnLocationSelected();


	protected void SnapBuildingTo(Tile _tile) {
		Tile[] tmpNewTiles = Tile.mouseOver.TryGetNeighboursArea(size);
		///we snap building only if it can fit into the new tiles

		if (tmpNewTiles != null) {
			if (tileOver != null) {
				Tile[] tmpOldTiles = tileOver.TryGetNeighboursArea(size);
				foreach (Tile tmpTile in tmpOldTiles) {
					if (tmpTile != null)
						tmpTile.SetHovered(false);
				}
			}

			foreach (Tile tmpTile in tmpNewTiles) {
				if (tmpTile != null)
					tmpTile.SetHovered(true);

			}
			tileOver = Tile.mouseOver;
			building.transform.localPosition = tileOver.transform.localPosition;
		}
	}
}
