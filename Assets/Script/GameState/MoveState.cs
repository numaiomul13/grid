﻿using UnityEngine;

public class MoveState : BuildingInteractionState {

	BuildingLogic buildLogic;
	Tile initialPos;
	bool buildingMoved;


	public MoveState(BuildingLogic _building) {
		buildLogic = _building;
		building = _building.mono.gameObject;
		building.GetComponent<BoxCollider>().enabled = false;//this will allow mouse to hit the grid for better building placement
		size = _building.data.size;
		initialPos = BuildingsInventory.GetPivot(_building);
		tileOver = initialPos;

		Tile[] tmpOldTiles = initialPos.TryGetNeighboursArea(size, true);
		foreach (Tile tmpTile in tmpOldTiles) {
			tmpTile.SetHovered(false);
			tmpTile.buildingRef = null;
		}
	}

	public override void OnExit() {
		base.OnExit();
		building.GetComponent<BoxCollider>().enabled = true;
		if (!buildingMoved) {
			SnapBuildingTo(initialPos);
			BuildingsInventory.Move(buildLogic, initialPos);
		}
	}

	protected override void OnLocationSelected() {
		buildingMoved = true;
		BuildingsInventory.Move(buildLogic, tileOver);
	}
}
