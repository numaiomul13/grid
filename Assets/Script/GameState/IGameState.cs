﻿public interface IGameState
{
    void OnEnter();
    void UpdateLoop(float _deltaTime);
    void OnExit();
}
