﻿public class BuildingLogic {
	public BuildingData data { get; private set; }
	public BuildingMono mono { get; private set; }

	public float Counter { get; private set; }
	bool IsAutoProd {
		get {
			return data.prod.isAuto;
		}
	}
	public enum STATE {
		CONSTRUCTION,
		STANDBY,
		PRODUCTION
	}

	public STATE State { get; private set; }
	static BuildingLogic uiOwner = null;

	public BuildingLogic(BuildingData _data, BuildingMono _mono, LoadInfo _loadInfo = null) {
		data = _data;
		mono = _mono;
		uiOwner = null;
		mono.Init(this);
		if (_loadInfo == null) {
			State = STATE.CONSTRUCTION;
			Counter = 0;
		} else {
			State = (STATE)System.Enum.Parse(typeof(STATE), _loadInfo.state);
			Counter = _loadInfo.counter;
		}
		if (State == STATE.CONSTRUCTION || (data.prod != null && State == STATE.PRODUCTION)) {
			GameState.Register(Update);
		}
	}

	public void OnSelect() {
		if (GameState.GetState() == typeof(CameraMove.ViewState)) {
			BuildingPopUp.Open(data.name, StartProduction);
			uiOwner = this;
			UpdateUI();
		} else if (GameState.GetState() == typeof(SelectBuildingState)) {
			GameState.SetState(new MoveState(this));
		}
	}

	void Update(float _deltaTime) {
		Counter += _deltaTime;
		switch (State) {
			case STATE.CONSTRUCTION:
				if (Counter > data.cost.time) {
					Counter = 0;
					if (data.prod == null) {
						State = STATE.STANDBY;
						GameState.Unregister(Update);
					} else {
						if (IsAutoProd) {
							State = STATE.PRODUCTION;
						} else {
							State = STATE.STANDBY;
							GameState.Unregister(Update);
						}
					}
				}
				break;
			case STATE.PRODUCTION:
				while (Counter > data.prod.time) {
					Counter -= data.prod.time;
					ResourcesLogic.Add(data.prod.value);
					if (!IsAutoProd) {
						State = STATE.STANDBY;
						Counter = 0;
						GameState.Unregister(Update);
					}
				}
				break;
			default:
				UnityEngine.Debug.LogWarning("We are registered for timer and do nothing with it", mono);
				break;
		}
		UpdateUI();
	}

	void UpdateUI() {
		if (uiOwner == this) {
			if (!BuildingPopUp.IsOpen()) {
				uiOwner = null;
				return;
			}

			if (data.prod == null) {
				BuildingPopUp.UpdateInfo("", -1);
			} else {
				switch (State) {
					case STATE.CONSTRUCTION:
						BuildingPopUp.UpdateInfo("Constructing...", Counter / data.cost.time);
						break;
					case STATE.PRODUCTION:
						BuildingPopUp.UpdateInfo("Producing...", Counter / data.prod.time);
						break;
					case STATE.STANDBY:
						BuildingPopUp.UpdateInfo("On stand-by", -1, true);
						break;
					default:
						throw new System.Exception("Building state not recognized:" + State);
				}
			}
		}
	}

	void StartProduction() {
		State = STATE.PRODUCTION;
		GameState.Register(Update);
	}

	public class LoadInfo {
		public string state;
		public float counter;
	}
}
