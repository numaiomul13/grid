﻿using UnityEngine;
using UnityEngine.EventSystems;

//A Bridge between Unity Gameobjects and BuildingLogic
public class BuildingMono : MonoBehaviour {
	public BuildingLogic logic;
	public GameObject popUpPrefab;

	float mouseClickTimer;

	private void Awake() {
		enabled = false;
	}

	public void Init(BuildingLogic _logic) {
		logic = _logic;
	}

	private void Update() {
		mouseClickTimer += Time.deltaTime;
	}

	//for a succesfull click, mouse click should start inside building, exit inside building and elapsedTime should be small (less than 0.1)
	public void OnMouseDown() {
		enabled = true;
		mouseClickTimer = 0f;
	}
	public void OnMouseUp() {
		if (mouseClickTimer < 0.1f && enabled) {
			logic.OnSelect();
		}
		enabled = false;
	}
}
