﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class BuildMenu : MonoBehaviour {

	public BuildingUIItem uiPrefab;
	public Transform contentHolder;
	public UnityEvent onOpen;
	public UnityEvent onClose;

	Dictionary<int, BuildingUIItem> items;

	private void Awake() {
		items = new Dictionary<int, BuildingUIItem>();
		foreach (KeyValuePair<BuildingData, int> tmpInfo in BuildingsInventory.GetBuildingsInfo()) {
			BuildingUIItem tmpItem = Instantiate(uiPrefab, contentHolder);
			tmpItem.Init(tmpInfo.Key, () => Build(tmpInfo.Key.id));
			items.Add(tmpInfo.Key.id, tmpItem);
		}
	}

	public void OnOpenBtn(bool _value) {
		if (_value) {
			onOpen.Invoke();
			GameState.SetState(new UiState());
			ResourcesLogic.Register(OnResourceChange);
			OnResourceChange(0f, 0f, 0f);
		} else {
			onClose.Invoke();
			ResourcesLogic.Unregister(OnResourceChange);
		}
	}

	public void OnMoveBtn(bool _value) {
		if (_value) {
			GameState.SetState(new SelectBuildingState());
		}
	}

	public void OnRegularBtn (bool _value) {
		if (_value) {
			GameState.SetState(new CameraMove.ViewState());
		}
	}

	public void OnResourceChange(float _gold, float _wood, float _steel) {
		foreach (KeyValuePair<BuildingData, int> tmpInfo in BuildingsInventory.GetBuildingsInfo()) {
			items[tmpInfo.Key.id].UpdateAvailability(tmpInfo.Key, tmpInfo.Value, ResourcesLogic.CanRemove(tmpInfo.Key.cost.GetValue()));
		}
	}
	void Build(int _id) {
		onClose.Invoke();
		GameState.SetState(new BuildState(BuildingsInventory.GetBuildingData(_id)));
	}
}
