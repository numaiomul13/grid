﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class BuildingPopUp : MonoBehaviour {

	private static BuildingPopUp instance;

	public delegate void OnProgressBtn();

	public Text buildingName;
	public Text progressType;
	public Image progressBar;
	public Button produce;
	public Image backGround;

	void Awake() {
		if (instance != null) {
			throw new System.ApplicationException("Another instance already exists!");
		}
		instance = this;
		gameObject.SetActive(false);

	}

	public static void Open(string _name, UnityAction _callback) {
		GameState.SetState(new UiState());
		instance.buildingName.text = _name;
		instance.gameObject.SetActive(true);
		instance.produce.onClick.AddListener(_callback);
	}

	/// <summary>
	/// Update tooltip info
	/// </summary>
	/// <param name="_state">Current state of building</param>
	/// <param name="_progress">value between 0 and 1 EXCEPT when we want to hide the progressBar</param>
	/// <param name="_callback">callback to be called when PRODUCE btn is clicked, will show btn only when callback != null</param>
	public static void UpdateInfo(string _state, float _progress, bool _showBtn = false) {
		instance.progressType.text = _state;
		if (_progress >= 0 && _progress <= 1) {
			instance.progressBar.transform.parent.gameObject.SetActive(true);
			instance.progressBar.fillAmount = _progress;
			instance.produce.gameObject.SetActive(false);
		} else if (_showBtn) {
			instance.progressBar.transform.parent.gameObject.SetActive(false);
			instance.produce.gameObject.SetActive(true);
		} else {
			instance.progressBar.transform.parent.gameObject.SetActive(false);
			instance.produce.gameObject.SetActive(false);
		}
	}

	public static bool IsOpen() {
		return instance.gameObject.activeSelf;
	}


	public void OnCloseBtn() {
		gameObject.SetActive(false);
		GameState.SetState(new CameraMove.ViewState());
		produce.onClick.RemoveAllListeners();
	}
}
