﻿using SimpleJSON;
using UnityEngine;

/// <summary>
/// READONLY class, feel free to pass the reference
/// </summary>
public class BuildingData {
	public readonly int id;
	public readonly string name;
	public readonly int limit;
	public readonly Vector2Int size;
	public readonly string resourcePath;
	public readonly Cost cost;
	public readonly Production prod;
	public BuildingData(JSONObject _info) {
		id = _info["id"].AsInt;
		name = _info["name"].Value;
		limit = _info["limit"].AsInt;
		int tmpSize = _info["size"].AsInt;
		size = new Vector2Int(tmpSize / 10, tmpSize % 10);
		resourcePath = _info["resourcePath"].Value;
		if (_info["cost"] != null) {

			string[] tmpCosts = _info["cost"]["value"].Value.Split('|');
			Resource[] tmpCostArr = new Resource[tmpCosts.Length];
			for (int i = 0; i < tmpCosts.Length; i++) {
				tmpCostArr[i] = new Resource(tmpCosts[i]);
			}
			cost = new Cost(_info["cost"]["time"].AsFloat, tmpCostArr);

		}
		if (_info["production"] != null) {
			prod = new Production(
				_info["production"]["time"].AsFloat,
				new Resource(_info["production"]["value"].Value),
				_info["production"]["auto"].AsBool
			);
		}

	}
	public bool HasBuildLimit() {
		return limit != -1;
	}

	///Since id is unique, might as well use it for hashCode
	public override int GetHashCode() {
		return id;
	}
	public override bool Equals(object obj) {
		BuildingData tmpObj = obj as BuildingData;
		if (tmpObj == null) {
			return false;
		}
		return id.Equals(tmpObj.id);
	}

	public class Cost {
		public readonly float time;
		private Resource[] value;

		public Cost(float _time, Resource[] _value) {
			time = _time;
			value = _value;
		}
		public Resource[] GetValue() {
			return (Resource[])value.Clone();
		}
	}

	public class Production {
		public readonly float time;
		public readonly bool isAuto;
		//we do this to make sure the values we set can not be corrupted
		public readonly Resource value;

		public Production(float _time, Resource _value, bool _isAuto) {
			time = _time;
			value = _value;
			isAuto = _isAuto;
		}
	}
}

