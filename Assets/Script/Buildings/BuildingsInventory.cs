﻿using UnityEngine;
using SimpleJSON;
using System.Collections.Generic;

public class BuildingsInventory {
	
	static BuildingsInventory instance;

	Dictionary<BuildingData, int> buildingsCount;
	List<BuildingStorage> activeBuildings;

	GridCreator gridRef;
	public const bool CLEAR_LOAD = false;
	public const string PREFS_KEY = "buildingsSave";

	public static void Start() {
		if (instance != null) {
			throw new System.ApplicationException("Another instance already exists!");
		}
		instance = new BuildingsInventory();
		instance.gridRef = GameObject.FindObjectOfType<GridCreator>();//fastest way to gain access to the grid without making a monoBehaviour bridge

		TextAsset tmpData = Resources.Load<TextAsset>("Buildings/available");
		JSONArray tmpBuildingAvailable = JSON.Parse(tmpData.text).AsArray;
		instance.buildingsCount = new Dictionary<BuildingData, int>();
		foreach (JSONObject tmpJson in tmpBuildingAvailable) {
			instance.buildingsCount.Add(new BuildingData(tmpJson), 0);
		}
		instance.activeBuildings = new List<BuildingStorage>();
		if (CLEAR_LOAD) {
			PlayerPrefs.DeleteKey(PREFS_KEY);
		}
		instance.Load();
		
	}

	/// <summary>
	/// returns a copy of available buildings
	/// </summary>
	public static Dictionary<BuildingData, int> GetBuildingsInfo() {
		return new Dictionary<BuildingData, int>(instance.buildingsCount);
	}
	public static BuildingData GetBuildingData(int _id) {
		foreach (BuildingData tmp in instance.buildingsCount.Keys) {
			if (tmp.id == _id) {
				return tmp;
			}
		}
		return null;
	}

	public static void Build(BuildingMono _mono,BuildingData _data, Tile _pivot, Vector2Int _size, BuildingLogic.LoadInfo _loadInfo = null) {
		instance.buildingsCount[GetBuildingData(_data.id)]++;
		BuildingLogic tmpLogic = new BuildingLogic(_data, _mono, _loadInfo);
		instance.activeBuildings.Add(new BuildingStorage() { logic = tmpLogic, pivot = _pivot });
		;
				
		Tile[] tmpTiles = _pivot.TryGetNeighboursArea(_size);
		foreach (Tile tmpTile in tmpTiles) {
			tmpTile.buildingRef = _mono;
		}
		//loading game uses the same method and we want to avoid useless saves at the start of the game
		if (_loadInfo == null) {
			ResourcesLogic.Remove(_data.cost.GetValue());
			instance.Save();
		}
	}
	public static void Move(BuildingLogic _building, Tile _newPos) {
		BuildingStorage found = instance.activeBuildings.Find(x => x.logic == _building);
		found.pivot = _newPos;
		Tile[] tmpTiles = _newPos.TryGetNeighboursArea(_building.data.size);
		foreach (Tile tmpTile in tmpTiles) {
			tmpTile.buildingRef = _building.mono;
		}
		instance.Save();
	}
	public static Tile GetPivot(BuildingLogic _building) {
		return instance.activeBuildings.Find(x => x.logic == _building).pivot;
	}

	void Save() {
		//we save as json so it's easy to export to file/server etc
		JSONArray tmpArray = new JSONArray();
		JSONObject tmpItem;
		foreach (BuildingStorage tmpBuilding in activeBuildings) {
			tmpItem = new JSONObject();
			tmpItem.Add("id", tmpBuilding.logic.data.id.ToString());
			tmpItem.Add("posX", tmpBuilding.pivot.GetPos().x);
			tmpItem.Add("posY", tmpBuilding.pivot.GetPos().y);
			tmpItem.Add("state",tmpBuilding.logic.State.ToString());
			tmpItem.Add("counter",tmpBuilding.logic.Counter.ToString());
			tmpArray.Add(tmpItem);
		}
		PlayerPrefs.SetString(PREFS_KEY, tmpArray.ToString());
	}
	void Load() {
		if (PlayerPrefs.HasKey(PREFS_KEY)) {
			JSONArray tmpArray = JSON.Parse(PlayerPrefs.GetString(PREFS_KEY)).AsArray;
			foreach (JSONObject tmpItem in tmpArray) {
				int tmpId = tmpItem["id"].AsInt;
				int posX = tmpItem["posX"].AsInt;
				int posY = tmpItem["posY"].AsInt;
				BuildingData tmpData = GetBuildingData(tmpId);

				Tile tmpPivot = gridRef.GetAt(posX, posY);
				GameObject tmpGO = GameObject.Instantiate(Resources.Load<GameObject>("Buildings/" + tmpData.resourcePath));
				BuildingMono tmpMono = tmpGO.AddComponent<BuildingMono>();
				tmpGO.transform.localPosition = tmpPivot.transform.localPosition;
				Build(tmpMono, tmpData, gridRef.GetAt(posX, posY), tmpData.size, new BuildingLogic.LoadInfo(){state = tmpItem["state"].Value, counter = tmpItem["counter"].AsFloat});


			}
		}
	}

	class BuildingStorage {
		public BuildingLogic logic;
		public Tile pivot;
	}

}
