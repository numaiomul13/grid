﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Events;

public class BuildingUIItem : MonoBehaviour {
	public Text buildingName;
	public Text cost;
	public Text count;
	public Button btn;

	public void Init(BuildingData _info, UnityAction _onBuyBtn) {
		buildingName.text = _info.name;
		string tmpCost = "";
		foreach (Resource res in _info.cost.GetValue()) {
			tmpCost += res.ToString() + "  ";
		}
		cost.text = tmpCost;
		btn.onClick.AddListener(_onBuyBtn);
	}

	public void UpdateAvailability(BuildingData _info, int _count, bool _canBuy) {
		bool canBuy = true;
		if (_info.HasBuildLimit()) {
			count.text = _count + "/" + _info.limit;
			if (_count >= _info.limit) {
				canBuy = false;
			}
		} else {
			count.text = _count.ToString();
		}
		canBuy &= _canBuy;
		if (!canBuy) {
			count.color = Color.red;
			btn.interactable = false;
		} else {
			count.color = Color.green;
			btn.interactable = true;
		}
	}
}
